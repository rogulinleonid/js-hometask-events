'use strict';

/*
  Вам необходимо добавить обработчики событий в соотвествии с заданием.
  Вам уже даны готовые стили и разметка. Менять их не нужно,
  за исключением навешивания обработчиков и добавления data - аттрибутов
*/

fieldHeader.addEventListener('change', event => taskHeader.textContent = event.target.value)

document.addEventListener("click", event => {
  const { target } = event
  switch (target.className) {
    case 'menu-item':
      hideMenu()
      break
    case 'menu-btn':
      showMenu()
      break
    case 'field':
      moveBlock(event)
      break
    case 'remove':
      hideText(target)
      break
    case 'link':
      followingLink(event)
      break
      default:
        if (target.nodeName === 'A')
          if (!confirm('Вы уверены, что хотите перейти по ссылке?'))
            event.preventDefault()
  }
})



function hideMenu() {
  let menu = document.querySelector('#menu')
  menu.style.display = 'none'
}

function showMenu() {
  let menu = document.querySelector('#menu')
  menu.style.display = 'block'
}

// Я знаю, что тут пока происходит какая-то дичь :) 
function moveBlock(event) {
  let rect = event.target.getBoundingClientRect()
  let block = document.querySelector('.moved-block')
  block.style.left = `${event.clientX - rect.left}px`
  block.style.top = `${event.clientY - rect.top}px`
}

function hideText(target) {
  target.parentElement.style.display = 'none'
}

function folowingLink(event) {
  prompt('fads')
}

/**
 * Задание 1
 * Необходимо сделать выпадающее меню.
 * Меню должно открываться при клике на кнопку,
 * закрываться при клике на кнопку при клике по пункту меню
*/

/**
 * Задание 2
 * Необходимо реализовать перемещение блока по клику на поле.
 * Блок должен перемещаться в место клика.
 * При выполнении задания вам может помочь свойство element.getBoundingClientRect()
*/

/**
 * Задание 3
 * Необходимо реализовать скрытие сообщения при клике на крестик при помощи делегирования
*/

/**
 * Задание 4
 * Необходимо реализовать вывод сообщения при клике на ссылку.
 * В сообщении спрашивать, что пользователь точно хочет перейти по указанной ссылке.
 * В зависимости от ответа редиректить или не редиректить пользователя
*/

/**
 * Задание 5
 * Необходимо сделать так, чтобы значение заголовка изменялось в соответствии с измением
 * значения в input
*/
